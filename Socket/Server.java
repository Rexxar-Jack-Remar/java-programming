package Socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) throws Exception {

        String Server_file_path = System.getProperty("user.dir") + File.separator + "ServerFiles" + File.separator;
        System.out.println("当前工作路径：" + Server_file_path);

        /*1. 启动服务器，指定端口号为8888，等待客户端的连接*/
        ServerSocket ss = new ServerSocket(8888); //ServerSocket

        /*2. 接收客户端的连接请求，并返回通信套接字 */
        Socket cs = ss.accept(); //ClientSocket

        /*3.获取到输入输出流，接收客户端发来的数据或给客户端发送数据*/
        InputStream is = cs.getInputStream();
        DataInputStream dis = new DataInputStream(is);
        String file_name = dis.readUTF();
        File file = new File(Server_file_path + file_name);

        FileInputStream fis = null;
        OutputStream os = null;
        DataOutputStream dos = null;

        try {
            os = cs.getOutputStream();
            dos = new DataOutputStream(os);
            if (file.exists()) {

                /*告知客户端文件已找到*/
                System.out.println("======== 开始传输文件 ========");
                dos.writeBoolean(true);
                dos.flush();

                /*传输文件的大小*/
                long file_length = file.length();
                dos.writeLong(file_length);
                dos.flush();

                /*开始传输文件*/
                fis = new FileInputStream(file);
                byte[] bytes = new byte[1024];
                int length;
                long progress = 0;
                while ((length = fis.read(bytes, 0, bytes.length)) != -1) {
                    dos.write(bytes, 0, length);
                    dos.flush();
                    progress = progress + length;
                    System.out.print("传输进度：" + (100 * progress / file_length) + "%\r");
                }

                /*
                byte[] bytes0 = "".getBytes();
                System.out.println(bytes0.length);
                dos.write(bytes0);
                dos.flush();
                */

                System.out.println();
                System.out.println("======== 文件传输成功 ========");

                /*
                System.out.print("******");
                System.out.print(fis.read(bytes, 0, bytes.length));//输出-1，可以借此来跳出循环？
                System.out.println("******");
                */

            } else {
                dos.writeBoolean(false);
                dos.flush();
                System.out.println("======== 该文件不存在 ========");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            boolean status = dis.readBoolean();
            if (status) {
                System.out.println("======== 客户已经确认 ========");
            }

            if (fis != null)
                fis.close();
            if (dos != null)
                dos.close();
            cs.close();
        }

        /*4. 释放资源*/
        ss.close();
        is.close();
        if (os != null) {
            os.close();
        }

    }

}