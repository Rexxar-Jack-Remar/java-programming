# 搜索web

修改程序清单12-18，从某个URL（如http://cs.armstrong.edu.liang）开始搜索某个单词（例如，Computer Programming）。你的程序应提示用户输入单词以及起始URL，并且一单搜索到该单词则终止程序。显示包含该单词的页面URL地址。